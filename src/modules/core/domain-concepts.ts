export interface Channel {
  id: string;
  name: string;
  path: string;
  description: string;
  banner: string;
  subscribers: number;
}

export enum CommentType {
  reply,
  comment,
}
// Instead of the creating a Reply interface,
// a comment can be used to represent both
export interface Comment {
  id: string;
  fromName: string;
  fromEmail: string;
  body: string;
  timestamp: Date;
  replies: Comment[];
}

export interface Post {
  _id: number;
  imageUri?: string | null;
  title: string;
  content: string;
  channel: string;
  creator: User;
  votes: number;
  timestamp: number;
  comments: Comment[];
}

export interface User {
  id: string;
  name: string;
}
