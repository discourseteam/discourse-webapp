export enum PostMode {
  CREATE = 'create',
  COMMENT = 'comment',
  UPDATE = 'update',
  READ = 'read',
}
