import { Post, Comment } from '@/modules/core/domain-concepts';
import homeData from '@/data/home';
import sportsData from '@/data/sports';
import axios from 'axios';
import env from '@/config/env';

const postApi = (env as any).api.post;

export default new class PostService {
  public async getAll(channel: string): Promise<Post[]> {
    const x = true;
    return axios
      .get(`${postApi}`, {
        params: {
          channel,
        },
      })
      .then(result => {
        return result.data as Post[];
      });
  }

  public async getById(id: string): Promise<Post> {
    return axios
      .get<Post>(`${postApi}`, {
        params: { postId: id },
      })
      .then((result: any) => {
        return result.data as Post;
      });
  }

  public createPost(post: Post): Promise<string> {
    return axios.post(`${postApi}`, post).then((result: any) => {
      return result.data;
    });
  }

  public createComment(postId: string, comment: Comment) {
    return axios
      .put(`${postApi}`, {
        postId,
        comment,
      })
      .then((result: any) => {
        return result.data;
      });
  }
}();
