import channelData from '@/data/channels';

export default new class FeedService {
  public async fetch() {
    return Promise.resolve(channelData);
  }
}();
