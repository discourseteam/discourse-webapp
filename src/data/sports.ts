// tslint:disable
import { getRandomNumber } from "@/config/utils";

export default [
  {
    id: getRandomNumber(),
    title: "TFD: Build webapps",
    content: "some content",
    channel: {
      id: Math.random().toString(),
      name: "Sports",
      path: "/sports",
      description:
        "Your Sports channel description, view posts tailored to your general interests here.",
      banner: "banner-1.gif"
    },
    comments: [
      {
        fromName: "Patrick Harty",
        fromEmail: "pharty@exmaple.com",
        body: `Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum`,
        timestamp: Date.now()
      }
    ],
    creator: { id: Math.random().toString(), name: "Chatterbox" },
    votes: 1000,
    timestamp: Date.now()
  },
  {
    id: getRandomNumber(),
    title: "The impact of AI on traditional software",
    content: "some content",
    channel: {
      id: Math.random().toString(),
      name: "Sports",
      path: "/sports",
      description:
        "Your discourse channel description, view posts tailored to your general interests here.",
      banner: "banner-1.gif"
    },
    comments: [],
    creator: { id: Math.random().toString(), name: "Chatterbox" },
    votes: 1000,
    timestamp: Date.now()
  },
  {
    id: getRandomNumber(),
    title: "The impact of AI on traditional software",
    content: "some content",
    channel: {
      id: Math.random().toString(),
      name: "Sports",
      path: "/sports",
      description:
        "Your discourse channel description, view posts tailored to your general interests here.",
      banner: "banner-1.gif"
    },
    comments: [
      {
        fromName: "Andre Vidal",
        fromEmail: "avidal@exmaple.com",
        body: `But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was
            born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,
        timestamp: Date.now()
      },
      {
        fromName: "Carl Clarke",
        fromEmail: "cclarke@exmaple.com",
        body: `On the other hand, we denounce with righteous indignation and dislike men who are so beguiled`,
        timestamp: Date.now()
      },
      {
        fromName: "Tajay Linton",
        fromEmail: "tlinton@exmaple.com",
        body: `On the other hand.`,
        timestamp: Date.now()
      },
      {
        fromName: "Simmone Chin",
        fromEmail: "schin@exmaple.com",
        body: `If denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,
        timestamp: Date.now()
      }
    ],
    creator: { id: Math.random().toString(), name: "Chatterbox" },
    votes: 1000,
    timestamp: Date.now()
  },
  {
    id: getRandomNumber(),
    title: "The impact of AI on traditional software",
    content: "some content",
    channel: {
      id: Math.random().toString(),
      name: "Sports",
      path: "/sports",
      description:
        "Your discourse channel description, view posts tailored to your general interests here.",
      banner: "banner-1.gif"
    },
    comments: [
      {
        fromName: "Sharee Greenland",
        fromEmail: "sgreenland@exmaple.com",
        body: `But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,
        timestamp: Date.now()
      },
      {
        fromName: "Jason Scott",
        fromEmail: "jscott@exmaple.com",
        body: `On the other hand, we denounce with righteous indignation and dislike men who are so beguiled`,
        timestamp: Date.now()
      },
      {
        fromName: "Tajay Linton",
        fromEmail: "tlinton@exmaple.com",
        body: `On the other hand.`,
        timestamp: Date.now()
      },
      {
        fromName: "Simmone Chin",
        fromEmail: "schin@exmaple.com",
        body: `If denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,
        timestamp: Date.now()
      }
    ],
    creator: { id: Math.random().toString(), name: "Chatterbox" },
    votes: 1000,
    timestamp: Date.now()
  },
  {
    id: getRandomNumber(),
    title: "The impact of AI on traditional software",
    content: "some content",
    channel: {
      id: Math.random().toString(),
      name: "Sports",
      path: "/sports",
      description:
        "Your discourse channel description, view posts tailored to your general interests here.",
      banner: "banner-1.gif"
    },
    comments: [],
    creator: { id: Math.random().toString(), name: "Chatterbox" },
    votes: 1000,
    timestamp: Date.now()
  }
];
