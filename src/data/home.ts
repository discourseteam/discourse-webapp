// tslint:disable
import { getRandomNumber } from "@/config/utils";
import { Post, CommentType } from "@/modules/core/domain-concepts";

export default [
  {
    id: getRandomNumber(),
    title: "I am Tim Meadows, taking on all challengers!",
    content:
      // tslint:disable-next-line:max-line-length
      `<p>You may know me from SNL, where I was one of the longest-running cast members and originated such characters as Leon Phelps, 'The Ladies Man.' You may also know me as Principal Duvall from "Mean Girls," Principal Glascott on "The Goldbergs," Mike the Mailman on "Bob's Burgers," Caleb on "Brooklyn Nine-Nine" and many more. You can currently see me as Detective Judd Tolbeck, a low level cop in the San Diego Police Department, in the comedy series "No Activity," which is now streaming on CBS All Access.</p>
      <p><a href="https://www.youtube.com/watch?v=SQ4NhEP36So">https://www.youtube.com/watch?v=SQ4NhEP36So</a></p>
      <p>Proof: <a href="https://i.redd.it/fezg5nphsw021.jpg">https://i.redd.it/fezg5nphsw021.jpg</a></p>
      <p>Edit: Well this has been fun. And I don't mean that sarcastically. Please watch No Activity on CBS
      All Access and I will see you down the road (or down the comments).</p>`,
    channel: {
      id: Math.random().toString(),
      name: "General",
      path: "/general",
      description:
        "Your discourse channel description, view posts tailored to your general interests here.",
      banner: "banner-1.gif"
    },
    comments: [
      {
        fromName: "Andre Vidal",
        fromEmail: "avidal@exmaple.com",
        // tslint:disable-next-line:max-line-length
        body: `But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,
        timestamp: Date.now(),
        replies: [
          {
            fromName: 'Davane Davis',
            fromEmail: 'ddavis@exmaple.com',
            body: `idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,    
          
          },
          {
            fromName: 'John Doe',
            fromEmail: 'jdoe@exmaple.com',
            body: `give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,    
          }
        ]
      },
      {
        fromName: "Carl Clarke",
        fromEmail: "cclarke@exmaple.com",
        body: `On the other hand, we denounce with righteous indignation and dislike men who are so beguiled`,
        timestamp: Date.now(),
        replies: [
          {
            fromName: 'Davane Davis',
            fromEmail: 'ddavis@exmaple.com',
            body: `idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,    
          },
        ]       
      },
      {
        fromName: "Tajay Linton",
        fromEmail: "tlinton@exmaple.com",
        body: `On the other hand.`,
        timestamp: Date.now(),
      },
    ],
    creator: { id: Math.random().toString(), name: "Chatterbox" },
    votes: 1000,
    timestamp: Date.now()
  },
  {
    id: getRandomNumber(),
    title: "TFD: Build webapps",
    content: "some content",
    channel: {
      id: Math.random().toString(),
      name: "General",
      path: "/general",
      description:
        "Your discourse channel description, view posts tailored to your general interests here.",
      banner: "banner-1.gif"
    },
    comments: [
      {
        fromName: "Real Decoy",
        fromEmail: "rdecoy@exmaple.com",
        body: `But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,
        timestamp: Date.now(),
        replies: [
          {
            fromName: 'Davane Davis',
            fromEmail: 'ddavis@exmaple.com',
            body: `idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,    
          
          },
          {
            fromName: 'John Doe',
            fromEmail: 'jdoe@exmaple.com',
            body: `give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,    
          }
        ]
      },
      {
        fromName: "Davane Davis",
        fromEmail: "ddavis@exmaple.com",
        body: `But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?`,
        timestamp: Date.now(),

      },
    ],
    creator: { id: Math.random().toString(), name: "Chatterbox" },
    votes: 1000,
    timestamp: Date.now()
  },
  {
    id: getRandomNumber(),
    title: "The impact of AI on traditional software",
    content: "some content",
    channel: {
      id: Math.random().toString(),
      name: "General",
      path: "/general",
      description:
        "Your discourse channel description, view posts tailored to your general interests here.",
      banner: "banner-1.gif"
    },
    comments: [
      {
        fromName: "Hubert Graham",
        fromEmail: "hgraham@exmaple.com",
        body: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum`,
        timestamp: Date.now(),
        replies: [
          {
            fromName: 'Davane Davis',
            fromEmail: 'ddavis@exmaple.com',
            body: `idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth`,    
          },
        ]
      }
    ],
    creator: { id: Math.random().toString(), name: "Chatterbox" },
    votes: 1000,
    timestamp: Date.now()
  }
];
