// tslint:disable
export default [
  {
    id: "/home",
    name: "Home",
    path: "/",
    description:
      "Your discourse channel description, view posts tailored to your general interests here.",
    banner: "banner-1.gif",
    subscribers: 1500
  },
  {
    id: "/sports",
    name: "Sports",
    path: "/sports",
    description:
      "Your Sports channel description, view posts tailored to your general interests here.",
    banner: "banner-2.gif",
    subscribers: 100
  }
];
