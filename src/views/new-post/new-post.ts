import Vue from 'vue';
import Component from 'vue-class-component';
import { Post } from '@/modules/core/domain-concepts';
import PostService from '@/services/post.service';
import SyncLoader from 'vue-spinner/src/SyncLoader.vue';
import TextEditorView from '@/components/text-editor/text-editor.vue';

@Component({
  components: {
    SyncLoader,
    'text-editor': TextEditorView,
  },
  name: 'new-post',
})
export default class NewPostView extends Vue {
  // --------------------------------------------------------------------------
  // Fields
  // --------------------------------------------------------------------------
  private post: Post | null = null;

  // --------------------------------------------------------------------------
  // Handlers
  // --------------------------------------------------------------------------

  public onCreatePost(data: any) {
    // console.log(data);
    // this.post = data.post;
  }
}
