import { Channel } from '@/modules/core/domain-concepts';

import channelService from '@/services/channel.service';
import channelData from '@/data/channels';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Watch } from 'vue-property-decorator';
import ChannelInfoView from '@/components/channel-info/channel-info.vue';
import TrendingChannelsView from '@/components/trending-channels/trending-channels.vue';
import NavigationView from '@/components/navigation/navigation.vue';

@Component({
  components: {
    'channel-info': ChannelInfoView,
    'trending-channels': TrendingChannelsView,
    'navigation': NavigationView,
  },
  name: 'app',
})
export default class AppView extends Vue {
  // --------------------------------------------------------------------------
  // Fields
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Handlers
  // --------------------------------------------------------------------------

  public onCreateClick(data: any) {
    // create post button clicked
    // console.log(data);
    const channel = data.channel;
    this.navTo(
      (channel.path === '/' ? '/general' : channel.path) + '/post/new',
    );
  }

  public onSubscribeClick(data: any) {
    // subscribe button clicked
    // console.log(data);
  }

  // --------------------------------------------------------------------------
  // Private Methods
  // --------------------------------------------------------------------------

  private navTo(path: string) {
    // navigate to path specified
    // console.log(path);
    this.$router.push({ path });
  }

  // --------------------------------------------------------------------------
  // Hooks
  // --------------------------------------------------------------------------

  private mounted() {
    const routePath = this.$route.path;

    channelService.fetch().then(response => {
      // get channels from service and update store
      this.$store.dispatch('channel/setChannels', response);
    });
  }
}
