import Vue from 'vue';
import Component from 'vue-class-component';
import { Post, User } from '@/modules/core/domain-concepts';
import postService from '@/services/post.service';
import SyncLoader from 'vue-spinner/src/SyncLoader.vue';
import TextEditorView from '@/components/text-editor/text-editor.vue';

@Component({
  components: {
    SyncLoader,
    'text-editor': TextEditorView,
  },
  name: 'edit-post',
})
export default class EditPostView extends Vue {
  // --------------------------------------------------------------------------
  // Fields
  // --------------------------------------------------------------------------

  private post: Post | null = null;
  private userInitials: string = '';

  // --------------------------------------------------------------------------
  // Handlers
  // --------------------------------------------------------------------------

  public onUpdatePost(data: any) {
    // console.log(data);
    // this.post = data.post;
  }

  public onCancelPost() {
    this.$router.go(-1);
  }

  // --------------------------------------------------------------------------
  // Private Methods
  // --------------------------------------------------------------------------

  private async fetchPostDetails() {
    try {
      const postId = this.$route.params.postId;
      const post = await postService.getById(postId);
      this.post = post;
      this.getUserInitials(
        this.post !== null ? (this.post as any).creator : null,
      );
    } catch (e) {
      // console.error('PostId not an integer.');
    }
  }

  private getUserInitials(user: User) {
    if (user !== null) {
      const name = user.name;
      const nameArray = name.split(' ');
      const len = nameArray.length;
      let initials = name.charAt(0);
      if (len > 1) {
        initials += nameArray[len - 1].charAt(0);
      }
      initials = initials.toUpperCase();
      this.userInitials = initials;
    }
  }

  // --------------------------------------------------------------------------
  // Hooks
  // --------------------------------------------------------------------------

  private async mounted() {
    this.fetchPostDetails();
  }
}
