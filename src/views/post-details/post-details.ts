import Vue from 'vue';
import Component from 'vue-class-component';
import { Post, User, Comment } from '@/modules/core/domain-concepts';
import postService from '@/services/post.service';
import SyncLoader from 'vue-spinner/src/SyncLoader.vue';
import CommentThreadView from '@/components/comment-thread/comment-thread.vue';
import TextEditorView from '@/components/text-editor/text-editor.vue';

@Component({
  components: {
    SyncLoader,
    'text-editor': TextEditorView,
    'comment-thread': CommentThreadView,
  },
  name: 'post-details',
})
export default class PostDetailsView extends Vue {
  // --------------------------------------------------------------------------
  // Inputs
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Fields
  // --------------------------------------------------------------------------

  private post: Post | any = null;
  private loading: boolean = true;
  private showClapAnim: boolean = false;
  private showClapAnimBounce: boolean = false;
  private userInitials: string = '';
  private comments: Comment[] = [];
  private clapAnimTimeout: any = null;
  private clapCount = 0;
  // --------------------------------------------------------------------------
  // Accessors
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  constructor() {
    super();
  }

  // --------------------------------------------------------------------------
  // Handlers
  // --------------------------------------------------------------------------

  public onEditClick() {
    const { channel } = this.post;
    const path = `${channel}/details/${this.post._id}/edit`;
    this.$router.push({ path });
  }

  public onClapClick() {
    this.clapCount = this.clapCount >= 9 ? 9 : this.clapCount+=1;
    if (this.clapAnimTimeout !== null) {
      clearTimeout(this.clapAnimTimeout);
      this.showClapAnim = true;
    }

    this.clapAnimTimeout =  setTimeout(() => {
      this.showClapAnim = false;
      this.clapAnimTimeout = null;
    }, 1000);


    this.showClapAnim = true;

    

  }

  public coCommentClick() {
    //
  }
  // --------------------------------------------------------------------------
  // Private Methods
  // --------------------------------------------------------------------------

  private async fetchPostDetails() {
    try {
      const { postId } = this.$route.params;
      const post = await postService.getById(postId);

      if (post !== null) {
        this.post = post;
        this.comments = post.comments;
        this.getUserInitials((post as any).creator);
      }
    } catch (e) {
      console.error('PostDetailsView.fetchPostDetails(): ', e.message);
    }
  }

  private getUserInitials(user: User) {
    if (user !== null && user !== undefined) {
      const name = user.name || '';
      const nameArray = name.split(' ');
      const len = nameArray.length;
      let initials = name.charAt(0);
      if (len > 1) {
        initials += nameArray[len - 1].charAt(0);
      }
      initials = initials.toUpperCase();
      this.userInitials = initials;
    }
  }

  // --------------------------------------------------------------------------
  // Hooks
  // --------------------------------------------------------------------------

  private mounted() {
    setTimeout(() => {
      this.loading = false;
    }, 3000);
    this.fetchPostDetails();
  }
}
