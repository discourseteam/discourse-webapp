import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop, Watch, Emit } from 'vue-property-decorator';
import { Post } from '@/modules/core/domain-concepts';
import SyncLoader from 'vue-spinner/src/SyncLoader.vue';
import PostView from '@/components/post/post.vue';
import postService from '@/services/post.service';
import store from '@/config/store';

@Component({
  components: {
    SyncLoader,
    post: PostView,
  },
  name: 'Feed',
})
export default class FeedView extends Vue {
  // --------------------------------------------------------------------------
  // Inputs
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Fields
  // --------------------------------------------------------------------------

  private loading: boolean = true;
  // --------------------------------------------------------------------------
  // Outputs
  // --------------------------------------------------------------------------
  get channel() {
    return this.$store.getters['channel/currentChannel'];
  }

  @Watch('channel')
  public onChannel(newValue: string, oldValue: string) {
    this.refresh();
  }
  // --------------------------------------------------------------------------
  // Accessors
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Handlers
  // --------------------------------------------------------------------------

  public onPostClick(post: Post) {
    const { channel } = post;
    const path = `${channel}/details/${post._id}`;
    this.$router.push({ path });
  }

  // --------------------------------------------------------------------------
  // Private Methods
  // --------------------------------------------------------------------------

  private async refresh() {
    const channel = this.$store.getters['channel/currentChannel'];

    if (channel !== null) {
      const posts = await postService.getAll(channel.id);
      this.$store.dispatch('post/setPosts', posts);
    }
  }

  // --------------------------------------------------------------------------
  // Hooks
  // --------------------------------------------------------------------------

  private mounted() {
    setTimeout(() => {
      this.loading = false;
    }, 3000);
    this.refresh();
  }
}
