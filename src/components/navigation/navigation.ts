import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop, Watch, Emit } from 'vue-property-decorator';
import { Channel } from '@/modules/core/domain-concepts';

import channelService from '@/services/channel.service';
import channelData from '@/data/channels';

@Component({
  name: 'navigation',
})
export default class NavigationView extends Vue {
  public showChannels: boolean = false; // hide channel dropdown by default
  public showSortBy: boolean = false; // hide sort dropdown by default
  public sortBy: string = 'Best'; // set default sort filter
  public sortOptions: string[] = ['Best', 'New', 'Rising']; // set list of sorting options

  // --------------------------------------------------------------------------
  // Inputs
  // --------------------------------------------------------------------------

  @Prop() public theme!: string;

  // --------------------------------------------------------------------------
  // Fields
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Outputs
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Accessors
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Handlers
  // --------------------------------------------------------------------------
  public navTo(path: string) {
    // navigate to path specified
    // console.log(path);
    this.$router.push({ path });
  }

  public onToggleView() {
    // switching between blog view and compact view
    this.$store.dispatch('misc/toggleCompact');
  }

  public onSetTheme(theme: string) {
    // switching between light and dark theme
    this.$store.dispatch('misc/setTheme', theme);
  }

  public onChannelChange(channel: Channel) {
    // switching between channels
    this.$store.dispatch('channel/setCurrentChannel', channel);
    this.navTo(channel.path);
  }

  public onSortBy(sortBy: string) {
    // sort list by (incomplete)
    this.sortBy = sortBy;
  }

  // --------------------------------------------------------------------------
  // Hooks
  // --------------------------------------------------------------------------
}
