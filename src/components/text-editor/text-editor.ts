import { PostMode } from '@/modules/core/post-mode';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop, Watch, Emit } from 'vue-property-decorator';
import { Post } from '@/modules/core/domain-concepts';
import { Channel } from '@/modules/core/domain-concepts';
import { capitalize } from '@/config/utils';

const { Editor, EditorContent, EditorMenuBar } = require('tiptap');
const {
  Blockquote,
  CodeBlock,
  HardBreak,
  Heading,
  OrderedList,
  BulletList,
  ListItem,
  TodoItem,
  TodoList,
  Bold,
  Code,
  Italic,
  Link,
  Strike,
  Underline,
  History,
} = require('tiptap-extensions');

@Component({
  components: {
    EditorContent,
    EditorMenuBar,
  },
  name: 'text-editor',
})
export default class TextEditorView extends Vue {
  // --------------------------------------------------------------------------
  // Inputs
  // --------------------------------------------------------------------------

  @Prop() public mode!: string;
  @Prop() public post!: Post | null;

  // --------------------------------------------------------------------------
  // Fields
  // --------------------------------------------------------------------------

  public editor: any = null;
  public html: string = '';
  public title: string = '';
  public channel: Channel | string = '';
  public imageUri: string | undefined = '';

  // --------------------------------------------------------------------------
  // Accessors
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Handlers
  // --------------------------------------------------------------------------

  public onChangeImage() {
    const uri = prompt('Enter your image url below: ', this.imageUri);
    this.imageUri = String((uri === null) ? '' : uri);
  }

  public onSubmit() {
    switch (this.mode) {
      case PostMode.CREATE:
        this.$emit('createPost', {
          channel: this.channel,
          title: this.title,
          content: this.html,
          imageUri: this.imageUri,
        });
        break;
      case PostMode.COMMENT:
        this.$emit('createPost', {
          content: this.html,
        });
        break;
      case PostMode.UPDATE:
        this.$emit('updatePost', {
          channel: this.channel,
          title: this.title,
          content: this.html,
          imageUri: this.imageUri,
        });
        break;
      default:
        throw new Error('TextEditorView.onSubmit(): Unknown text editor mode');
    }
  }

  public onClear() {
    this.html = '';
    this.editor.clearContent(true);
    this.editor.focus();
  }

  public onCancel() {
    this.$emit('cancelPost');
  }

  // --------------------------------------------------------------------------
  // Hooks
  // --------------------------------------------------------------------------

  private mounted() {
    // console.log(this.$route.path);
    this.channel = capitalize(this.$route.path.split('/')[1]);
    this.title = this.post ? this.post.title : '';
    this.imageUri = (this.post && this.post.imageUri) ? this.post.imageUri : '';
    this.editor = new Editor({
      extensions: [
        new Blockquote(),
        new CodeBlock(),
        new HardBreak(),
        new Heading({ levels: [1, 2, 3] }),
        new BulletList(),
        new OrderedList(),
        new ListItem(),
        new TodoItem(),
        new TodoList(),
        new Bold(),
        new Code(),
        new Italic(),
        new Link(),
        new Strike(),
        new Underline(),
        new History(),
      ],
      editable: this.mode !== 'read',
      content: this.post ? this.post.content : '',
      onUpdate: (response: any) => {
        this.html = response.getHTML();
      },
      onInit: (response: any) => {
        // console.log(response.state);
      },
    });
  }

  private beforeDestroy() {
    this.editor.destroy();
  }
}
