import Vue from 'vue';
import Component from 'vue-class-component';
import CommentView from '@/components/comment/comment.vue';
import PostService from '@/services/post.service';
import { Prop } from 'vue-property-decorator';
@Component({
  components: {
    comment: CommentView,
  },
  name: 'comment-thread',
})
export default class CommentThreadView extends Vue {
  // --------------------------------------------------------------------------
  // Inputs
  // --------------------------------------------------------------------------
  @Prop() public comments!: Comment | null;
  // --------------------------------------------------------------------------
  // Fields
  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------
  // Accessors
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Hooks
  // --------------------------------------------------------------------------
  private async mounted() {
    //
  }

  // --------------------------------------------------------------------------
  // Methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Outputs
  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------
  // Handlers
  // --------------------------------------------------------------------------
}
