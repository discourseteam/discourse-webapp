import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const channel = {
  namespaced: true,
  state: {
    all: [],
    current: null,
  },
  getters: {
    allChannels: (state: any) => {
      return state.all;
    },
    currentChannel: (state: any) => {
      return state.current;
    },
  },
  mutations: {
    setChannels(state: any, data: any) {
      state.all = data;
    },
    setCurrentChannel(state: any, data: any) {
      state.current = data;
    },
  },
  actions: {
    setChannels(context: any, data: any) {
      context.commit('setChannels', data);
      if (!context.getters.currentChannel) {
        context.commit('setCurrentChannel', data[0]);
      }
    },
    setCurrentChannel(context: any, data: any) {
      context.commit('setCurrentChannel', data);
      const currentChannel = context.getters['channel/currentChannel'];
      if(currentChannel === data) {
        context.dispatch('post/setPosts', [], {root: true});
      }
    },
  },
};

const post = {
  namespaced: true,
  state: {
    all: [],
    current: null,
  },
  getters: {
    allPosts: (state: any) => {
      return state.all;
    },
    currentPost: (state: any) => {
      return state.current;
    },
  },
  mutations: {
    setPosts(state: any, data: any) {
      state.all = data;
    },
    setCurrentPost(state: any, data: any) {
      state.current = data;
    },
  },
  actions: {
    setPosts(context: any, data: any) {
      context.commit('setPosts', data);
    },
    setCurrentPost(context: any, data: any) {
      context.commit('setCurrentPost', data);
    },
  },
};

const misc = { // miscellaneous
  namespaced: true,
  state: {
    theme: 'dark',
    compact: false,
  },
  getters: {
    theme: (state: any) => {
      return state.theme;
    },
    compact: (state: any) => {
      return state.compact;
    },
  },
  mutations: {
    setTheme(state: any, data: any) {
      state.theme = data;
    },
    toggleCompact(state: any) {
      state.compact = !state.compact;
    },
  },
  actions: {
    setTheme(context: any, data: any) {
      context.commit('setTheme', data);
    },
    toggleCompact(context: any) {
      context.commit('toggleCompact');
    },
  },
};

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    channel,
    post,
    misc,
  },
});
