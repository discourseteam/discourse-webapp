import Vue from 'vue';
import Router from 'vue-router';
import Feed from '@/views/feed/feed.vue';
import EditPost from '@/views/edit-post/edit-post.vue';
import NewPost from '@/views/new-post/new-post.vue';
import PostDetails from '@/views/post-details/post-details.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'mainFeed',
      component: Feed,
    },
    {
      path: '/:channel',
      name: 'otherFeed',
      component: Feed,
    },
    {
      path: '/:channel/details/:postId',
      name: 'post-details',
      component: PostDetails,
    },
    {
      path: '/:channel/details/:postId/edit',
      name: 'edit-post',
      component: EditPost,
    },
    {
      path: '/:channel/post/new',
      name: 'new-post',
      component: NewPost,
    },

    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () =>
    //     import(/* webpackChunkName: "about" */ '@/views/about.vue'),
    // },
    // {
    //   path: '/404',
    //   name: 'notFound',
    //   component: NotFound,
    //   meta: {requiresAuth: false}
    // },
    // {
    //   path: '/*',
    //   name: 'catchAll',
    //   redirect: { name: 'notFound' },
    // }
  ],
});
