import Vue from 'vue';
import AppView from '@/views/app/app.vue';
import router from '@/config/router';
import store from '@/config/store';
import '@/config/register-service-worker';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(AppView),
}).$mount('#app');
